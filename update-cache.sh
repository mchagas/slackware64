#!/bin/sh

# Figure out our root directory
#ROOTDIR=$(pwd)
#unset CHROOT
#if [ "${ROOTDIR}" != "/" ] ; then
#  ROOTDIR="${ROOTDIR}/"
#  CHROOT="chroot ${ROOTDIR} "
#fi

# update the applications menu database
if [ -x /usr/bin/update-desktop-database ] ; then
 /usr/bin/update-desktop-database -q /usr/share/applications 2> /dev/null
fi


# update the gtk-icon-cache
if [ -x /usr/bin/gtk-update-icon-cache ] ; then
  for dir in /usr/share/icons/* ; do
    if [ -d $dir ]; then
       /usr/bin/gtk-update-icon-cache --quiet -f -t $dir 2> /dev/null
    fi
  done
fi


# update the mime database
if [ -x /usr/bin/update-mime-database ] ; then
 /usr/bin/update-mime-database -q /usr/share/mime 2> /dev/null
fi
